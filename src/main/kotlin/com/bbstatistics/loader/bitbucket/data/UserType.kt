package com.bbstatistics.loader.bitbucket.data

enum class UserType {
    USER,
    TEAM
}
