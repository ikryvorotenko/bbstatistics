package com.bbstatistics.controller

import org.springframework.hateoas.Link
import org.springframework.hateoas.ResourceSupport

class LinksResource : ResourceSupport {
    constructor(vararg links: Link) {
        add(*links);
    }

    constructor(links: Iterable<Link>) {
        add(links);
    }
}