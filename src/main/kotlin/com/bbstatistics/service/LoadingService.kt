package com.bbstatistics.service

import com.bbstatistics.loader.PullRequestLoader
import com.bbstatistics.loader.RepoLoader
import com.bbstatistics.repository.PullRequestRepository
import com.bbstatistics.repository.RepoRepository
import com.bbstatistics.util.async
import com.google.common.base.Stopwatch
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service

@Service
@Scope("session")
open class LoadingService
@Autowired constructor(
        val pullRequestLoader: PullRequestLoader,
        val repoLoader: RepoLoader,

        val repoRepository: RepoRepository,
        val pullRequestRepository: PullRequestRepository
) {

    val log = LoggerFactory.getLogger(LoadingService::class.java.name)

    fun load(fullName: String) {
        val started = Stopwatch.createStarted()

        repoRepository.save(repoLoader.loadOne(fullName))

        val repoSize = pullRequestLoader.sizeInRepo(fullName)
        RepoLoadingProgressTracker.track(fullName, repoSize.toInt())

        async(1..repoSize) {
            loadPullRequest(it, fullName)
            RepoLoadingProgressTracker.increment(fullName)
        }

        val stopped = started.stop()
        log.info("Finished to load repo {}, took {}", fullName, stopped.toString())

        RepoLoadingProgressTracker.get(fullName)!!.finish(stopped.toString())
    }


    private fun loadPullRequest(pullRequestId: Long, repo: String) {
        try {
            pullRequestRepository.save(pullRequestLoader.loadOne(repo, pullRequestId))
        } catch(e: Exception) {
            log.error("Failed loading pull request $pullRequestId", e)
        }
    }

    fun refresh(fullName: String) {
        val started = Stopwatch.createStarted()

        val id = pullRequestRepository.lastPullRequestId(fullName)

        val size = pullRequestLoader.sizeInRepo(fullName)
        RepoLoadingProgressTracker.track(fullName, (size - id).toInt())

        async(id + 1..size) {
            loadPullRequest(it, fullName)
            RepoLoadingProgressTracker.increment(fullName)
        }

        val stopped = started.stop()
        RepoLoadingProgressTracker.get(fullName)!!.finish(stopped.toString())
    }

}
