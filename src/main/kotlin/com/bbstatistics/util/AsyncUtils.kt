package com.bbstatistics.util

import org.springframework.web.context.request.RequestContextHolder
import java.util.concurrent.Callable
import java.util.concurrent.Executors

fun async(method: () -> Unit) {
    val requestAttributes = RequestContextHolder.getRequestAttributes()

    Executors.newSingleThreadExecutor().execute({
        RequestContextHolder.setRequestAttributes(requestAttributes)
        method();
        RequestContextHolder.resetRequestAttributes()
    })

    RequestContextHolder.setRequestAttributes(requestAttributes)
}

fun <T, K> async(range: Iterable<K>, method: (i: K) -> T): List<T> {
    val requestAttributes = RequestContextHolder.getRequestAttributes()

    val callables = range.map {
        Callable<T> {
            RequestContextHolder.setRequestAttributes(requestAttributes)
            val result = method(it)
            RequestContextHolder.resetRequestAttributes()
            return@Callable result
        }
    }

    val resultCollection = Executors.newFixedThreadPool(25)
            .invokeAll(callables)
            .map {
                it.get()
            }

    RequestContextHolder.setRequestAttributes(requestAttributes)

    return resultCollection;
}

