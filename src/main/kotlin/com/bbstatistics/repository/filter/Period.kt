package com.bbstatistics.repository.filter

import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*

enum class Period {
    DAY {
        override fun ago(): Date {
            return Date.from(LocalDateTime.now().minusDays(1L).atZone(ZoneId.systemDefault()).toInstant());
        }
    },
    WEEK {
        override fun ago(): Date {
            return Date.from(LocalDateTime.now().minusWeeks(1L).atZone(ZoneId.systemDefault()).toInstant());
        }
    },
    MONTH {
        override fun ago(): Date {
            return Date.from(LocalDateTime.now().minusMonths(1L).atZone(ZoneId.systemDefault()).toInstant());
        }
    };

    companion object {
        fun forName(key: String): Period {
            return values().find { it.name.equals(key, true) }
                    ?: throw IllegalArgumentException("No value found for ${key}")
        }

    }

    abstract fun ago(): Date

}