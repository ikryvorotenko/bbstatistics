package com.bbstatistics.loader

import com.bbstatistics.data.PullRequest
import com.bbstatistics.data.Repo
import com.bbstatistics.data.User


interface RepoLoader {
    fun loadOne(fullName: String): Repo
    fun loadAll(): List<Repo>
    fun availableRepos(): List<String>
}

interface PullRequestLoader {
    fun loadOne(repo: String, pullRequestId: Long): PullRequest
    fun sizeInRepo(repo: String): Long
}

interface UserLoader {
    fun loadOne(username: String): User
}
