package com.bbstatistics.loader.bitbucket.data

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.TreeNode
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer

import java.io.IOException

class ContentJsonDeserializer : JsonDeserializer<String>() {
    @Throws(IOException::class)
    override fun deserialize(jp: JsonParser, ctxt: DeserializationContext): String {
        val treeNode = jp.codec.readTree<TreeNode>(jp)
        return jp.codec.readValue(treeNode.get("raw").traverse(), String::class.java)
    }
}
