package com.bbstatistics.loader.bitbucket.data.hal

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import java.util.*

open class LinkedResource {

    @JsonDeserialize(using = LinksDeserializer::class)
    val links: List<Link> = ArrayList()

    fun getLink(key: String): Link {
        return links.find({ it -> key == it.rel })
                ?: throw IllegalArgumentException("No links found by key " + key)
    }

    fun getHref(key: String): String {
        return getLink(key).href!!
    }

    companion object {
        val REL_SELF = "self"
    }
}
