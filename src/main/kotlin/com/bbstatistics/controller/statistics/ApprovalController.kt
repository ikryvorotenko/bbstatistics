package com.bbstatistics.controller.statistics

import com.bbstatistics.controller.MetaController
import com.bbstatistics.repository.PullRequestRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/stat/approval")
@Scope("request")
class ApprovalController @Autowired
constructor(var pullRequestRepository: PullRequestRepository) : MetaController() {

}