package com.bbstatistics.loader.bitbucket.data

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.TreeNode
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import java.io.IOException

class AuthorJsonDeserializer : JsonDeserializer<UserView>() {

    @Throws(IOException::class)
    override fun deserialize(jp: JsonParser, ctxt: DeserializationContext): UserView {
        val codec = jp.codec
        val treeNode = codec.readTree<TreeNode>(jp)

        val user = treeNode.get("user")
        if (user == null) {
            val raw = treeNode.get("raw")
            val username = raw.traverse(codec).readValueAs(String::class.java)
            return UserView(username, username)
        }

        val traverse = user.traverse(codec)
        return codec.readValue(traverse, UserView::class.java)
    }
}
