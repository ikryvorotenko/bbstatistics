package com.bbstatistics.loader.bitbucket.data

import com.bbstatistics.data.Approval
import com.bbstatistics.data.Comment
import com.bbstatistics.data.Update

class ActivityView {

    var comment: CommentView? = null

    var approval: ApprovalView? = null

    var update: UpdateView? = null

    fun ifComment(supplier: (Comment) -> Unit): ActivityView {
        comment?.let { supplier(it.toComment()) }
        return this
    }

    fun ifUpdate(supplier: (Update) -> Unit): ActivityView {
        update?.let { supplier(it.toUpdate()) }
        return this
    }

    fun ifApproval(supplier: (Approval) -> Unit): ActivityView {
        approval?.let { supplier(it.toApproval()) }
        return this
    }
}
