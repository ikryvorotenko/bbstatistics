This is an application for gathering different kind of statistics from bitbucket repositories.

## Statistics endpoints ##

Root: **/stat**

Each statistics endpoint has a `/dashboard` sub-endpoint, which is kind of aggregation of all available sub-endpoints within certain statistics endpoint. It takes all available sub-endpoints and pass the current filters from session. 

### Approval statistics ###

Root: **/approval**

* /by-user - aggregation of approvals done by users
* /comments-after-approve - aggregation of comments left on pull request after certain user approved it. This metric shows the quality of user's review, i.e. if they approve the pull request and after that there were 10 comments left, that means the user clicked approve without proper review. 

### Pull request statistics ###

Root: **/pullrequest**

* /comments-by - aggregation of comments left by users across pull requests
* /by-user - aggregation of pull requests by users
* /by-days - information of raised pull requests grouped by days 
* /merged-by - information about # of pull requests merged by users
* /comments-on - information about # of comments left on user's pull request 
* /duration - pull request duration information: including time from creation to merge
* /delays - pull request activity delays: based on the time difference between comments

### Comment statistics ###

Root: **/comment**

TDB

## Filters endpoints ##

There's a feature to set some filters to any statistics endpoint. Any of the filter passed to statistics request as a param would add restriction to query.

Also, there's an option to save this filters configuration, which will be used in all links to statistics. This configuration is stored in user's session and can be set/changed/reset by endpoints below.

Available filters:

* `name {possible-values} [restrictions]` 
* repository - filters the results for certain repository [repository is identified as {owner}/{reponame}]
* from - filters the result **after** certain date [ISO date format 'yyyy-MM-dd', such as '2011-12-03' or '2011-12-03+01:00']
* to - filters the result **before** certain date [ISO date format 'yyyy-MM-dd', such as '2011-12-03' or '2011-12-03+01:00']
* date - filters the result for certain date [ISO date format 'yyyy-MM-dd', such as '2011-12-03' or '2011-12-03+01:00']
* user - filters the result by user (bitbucket username is used)
* period - {DAY, WEEK, MONTH} - filters the result for the last day/week/month
* state {OPEN, MERGED, DECLINED} [pull request only] - filters the result by pull request state 

Root: **/filters**

* {Root} - returns currently set filters
* /set - set the filters by provided request params
* * /filters/set?repository=repo-owner/repo-name&period=week would set the filter to repo `repo-owner/repo-name` and created date between `week ago` and `today` 
* /update - override the filters by provided request params. Note: this doesn't reset filters, if you need to clean it up, use /clear
* * /filters/set?period=day would update the filter to created date between 'yesterday' and 'today'. All the rest filters would remain the same
* /clear - clears all filters

## Repo endpoints ##

Root: **/repo**

* {Root} or /list - returns the result of user's available repositories. The result contains the list of hyperlinks to /load or /refresh depends on whether the repo was already loaded or not.
* /load - trigger loading of repository and redirect to /track endpoint
* /refresh - trigger refreshing of repository and redirect to /track endpoint
* /track - returns the information about repo loading/refreshing progress. It has a nice progress bar :)