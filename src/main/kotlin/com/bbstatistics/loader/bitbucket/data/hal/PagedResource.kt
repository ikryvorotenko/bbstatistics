package com.bbstatistics.loader.bitbucket.data.hal

import java.util.*

class PagedResource<T>(
        var size: Long? = null,
        var page: Long? = null,
        var pagelen: Long? = null,
        var next: String? = null,
        var previous: String? = null,
        var values: Collection<T> = ArrayList()
)
