package com.bbstatistics.loader.bitbucket.data

import com.bbstatistics.data.User
import com.bbstatistics.loader.bitbucket.data.hal.LinkedResource
import com.fasterxml.jackson.annotation.JsonProperty

class UserView(
        @JsonProperty("username")
        var username: String,
        @JsonProperty("display_name")
        var displayName: String
) : LinkedResource() {

    companion object {
        val REL_AVATAR = "avatar"
    }

    fun toUser(): User {
        return User(username = username, displayName = displayName, avatar = getLink(REL_AVATAR).href)
    }

}
