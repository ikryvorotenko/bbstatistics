package com.bbstatistics.controller.statistics

import com.bbstatistics.controller.MetaController
import com.bbstatistics.data.Comment
import com.bbstatistics.repository.filter.Filters
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/stat/comment")
@Scope("request")
class CommentController constructor() : MetaController() {

}