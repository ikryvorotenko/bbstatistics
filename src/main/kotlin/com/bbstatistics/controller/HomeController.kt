package com.bbstatistics.controller

import org.springframework.hateoas.mvc.ControllerLinkBuilder
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = "/")
class HomeController {

    @RequestMapping
    fun meta(): LinksResource {
        return LinksResource(
                ControllerLinkBuilder.linkTo(StatisticsController::class.java).withRel("statistics"),
                ControllerLinkBuilder.linkTo(RepoController::class.java).withRel("repositories"),
                ControllerLinkBuilder.linkTo(FiltersController::class.java).withRel("filters")
        )
    }

}