package com.bbstatistics.loader.bitbucket.data.hal

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.core.TreeNode
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import java.io.IOException
import java.util.*

class LinksDeserializer : JsonDeserializer<List<Link>>() {
    @Throws(IOException::class)
    override fun deserialize(jp: JsonParser, ctxt: DeserializationContext): List<Link> {
        val result = ArrayList<Link>()

        val codec = jp.codec
        val treeNode = codec.readTree<TreeNode>(jp)
        val fieldNames = treeNode.fieldNames()

        while (fieldNames.hasNext()) {
            val fieldName = fieldNames.next()

            val field = treeNode.get(fieldName).traverse(codec)

            if (JsonToken.START_OBJECT != field.nextToken()) {
                continue
            }

            val link = field.readValueAs(LinkHref::class.java)
            result.add(Link(fieldName, link.href))
        }

        return result
    }
}

class LinkHref(
        @JsonProperty("href")
        var href: String) {
}