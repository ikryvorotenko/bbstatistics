package com.bbstatistics.loader.bitbucket

import com.bbstatistics.data.Approval
import com.bbstatistics.data.Comment
import com.bbstatistics.data.PullRequest
import com.bbstatistics.data.Repo
import com.bbstatistics.data.Update
import com.bbstatistics.data.User
import com.bbstatistics.loader.PullRequestLoader
import com.bbstatistics.loader.RepoLoader
import com.bbstatistics.loader.UserLoader
import com.bbstatistics.loader.bitbucket.data.ActivityView
import com.bbstatistics.loader.bitbucket.data.PullRequestView
import com.bbstatistics.loader.bitbucket.data.RepoView
import com.bbstatistics.loader.bitbucket.data.UserView
import com.bbstatistics.loader.bitbucket.data.hal.PagedResource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.ParameterizedTypeReference
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder.fromHttpUrl
import java.util.*

@Service
class BitbucketRepoLoader
@Autowired
constructor(restTemplate: RestTemplate) : PageLoader<RepoView>(
        restTemplate,
        object : ParameterizedTypeReference<RepoView>() {},
        object : ParameterizedTypeReference<PagedResource<RepoView>>() {}
), RepoLoader {

    override fun availableRepos(): List<String> {
        return loadAllPages("https://api.bitbucket.org/2.0/repositories?role=owner&role=admin&role=contributor&role=member")
                .map({ it.fullName })
    }

    override fun loadAll(): List<Repo> {
        return loadAllPages("https://api.bitbucket.org/2.0/repositories?role=owner&role=admin&role=contributor&role=member")
                .map({ toRepo(it) })
    }

    override fun loadOne(fullName: String): Repo {
        val load = load("https://api.bitbucket.org/2.0/repositories/${fullName}")
        return toRepo(load)
    }

    private fun toRepo(load: RepoView): Repo {
        with (load) {
            return Repo(
                    id = fullName,
                    uuid = uuid,
                    language = language,
                    createdOn = createdOn,
                    updatedOn = updatedOn,
                    isPrivate = isPrivate
            )

        }
    }
}


@Service
class BitbucketPullRequestLoader
@Autowired
constructor(restTemplate: RestTemplate,
        val activityLoader: BitbucketActivityLoader) :
        PageLoader<PullRequestView>(
                restTemplate,
                object : ParameterizedTypeReference<PullRequestView>() {},
                object : ParameterizedTypeReference<PagedResource<PullRequestView>>() {}
        ), PullRequestLoader {

    override fun sizeInRepo(repo: String): Long {
        return get(sizeUrl(repo), pageType).body.size!!
    }

    override fun loadOne(repo: String, pullRequestId: Long): PullRequest {
        val pullRequestView = get(getOneUrl(pullRequestId, repo), type).body

        val updates = ArrayList<Update>()
        val approvals = ArrayList<Approval>()
        val comments = ArrayList<Comment>()
        activityLoader.loadAll(repo, pullRequestId)
                .forEach { activity ->
                    activity
                            .ifUpdate({ updates.add(it) })
                            .ifApproval({ approvals.add(it) })
                            .ifComment({ comments.add(it) })
                }

        val pullRequest = pullRequestView.toPullRequest(repo, approvals, comments, updates)

        return pullRequest
    }

    private fun getOneUrl(pullRequestId: Long, repo: String) = fromHttpUrl(GET_PULL_REQUEST_URL).buildAndExpand(repo,
            pullRequestId).toUriString()

    private fun sizeUrl(repo: String) = fromHttpUrl(GET_ALL_PULL_REQUESTS + "&pagelen=1").buildAndExpand(repo).toUriString()

    companion object {
        val BASE_PULL_REQUESTS = "https://api.bitbucket.org/2.0/repositories/{fullName}/pullrequests"
        val GET_ALL_PULL_REQUESTS = BASE_PULL_REQUESTS + "?state=OPEN, MERGED, DECLINED"
        val GET_PULL_REQUEST_URL = BASE_PULL_REQUESTS + "/{id}"
    }
}


@Service
open class BitbucketActivityLoader
@Autowired
constructor(restTemplate: RestTemplate) : PageLoader<ActivityView>(
        restTemplate,
        object : ParameterizedTypeReference<ActivityView>() {},
        object : ParameterizedTypeReference<PagedResource<ActivityView>>() {}
) {

    open fun loadAll(repo: String, pullRequestId: Long): Collection<ActivityView> {
        val url = fromHttpUrl(BitbucketPullRequestLoader.GET_PULL_REQUEST_URL + "/activity")
                .buildAndExpand(repo, pullRequestId).toUriString()
        return loadAllPages(url)
    }
}

@Service
class BitbucketUserLoader
@Autowired
constructor(restTemplate: RestTemplate) : PageLoader<UserView>
(
        restTemplate,
        object : ParameterizedTypeReference<UserView>() {},
        object : ParameterizedTypeReference<PagedResource<UserView>>() {}
), UserLoader {
    override fun loadOne(username: String): User {
        return get("https://api.bitbucket.org/2.0/users/${username}", type)
                .body!!
                .toUser()
    }

}