package com.bbstatistics.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext
import org.springframework.security.oauth2.client.OAuth2ClientContext
import org.springframework.security.oauth2.client.OAuth2RestTemplate
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails

@Configuration
@ConditionalOnProperty("security.oauth2.client.singleUserClientId")
open class OauthConfiguration {

    @Value("\${security.oauth2.client.singleUserClientId}")
    var singleUserClientId: String = "8mBWbNw8eNN64LE6gF"

    @Bean
    @Primary
    @ConfigurationProperties("security.oauth2.client")
    open fun oauth2ResourceOwnerPasswordResource(): ResourceOwnerPasswordResourceDetails {
        val details = ResourceOwnerPasswordResourceDetails()
        details.clientId = singleUserClientId
        return details
    }

    @Bean
    open fun oauth2ClientContext(): OAuth2ClientContext {
        return DefaultOAuth2ClientContext(DefaultAccessTokenRequest())
    }

    @Bean
    @Primary
    open fun oauth2RestTemplate(
        oauth2ClientContext: OAuth2ClientContext,
        details: OAuth2ProtectedResourceDetails): OAuth2RestTemplate {
        val template = OAuth2RestTemplate(details,
            oauth2ClientContext)
        return template
    }


    @Bean
    open fun simpleOauth2RestTemplate(oauth2ClientContext: OAuth2ClientContext,
        details: ResourceOwnerPasswordResourceDetails): OAuth2RestTemplate {
        val template = OAuth2RestTemplate(details,
            oauth2ClientContext)
        return template
    }


}