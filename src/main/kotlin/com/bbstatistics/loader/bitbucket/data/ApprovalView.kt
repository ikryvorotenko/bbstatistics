package com.bbstatistics.loader.bitbucket.data

import com.bbstatistics.data.Approval
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import java.util.*

class ApprovalView {

    @JsonDeserialize(using = DateDeserializer::class)
    var date: Date? = null

    var user: UserView? = null

    fun toApproval(): Approval {
        return Approval(
                user = user!!.username,
                date = date!!
        )
    }
}
