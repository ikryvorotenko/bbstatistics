package com.bbstatistics.controller

import com.bbstatistics.loader.PullRequestLoader
import com.bbstatistics.loader.RepoLoader
import com.bbstatistics.repository.PullRequestRepository
import com.bbstatistics.repository.RepoRepository
import com.bbstatistics.service.LoadingService
import com.bbstatistics.service.RepoLoadingProgressTracker
import com.bbstatistics.util.async
import com.google.common.base.Preconditions
import com.google.common.base.Strings
import com.google.common.collect.ImmutableMap
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@Scope("request")
@RestController
@RequestMapping("/repo")
class RepoController @Autowired
constructor(var repoLoader: RepoLoader,
        val loadingService: LoadingService,
        val repoRepository: RepoRepository,
        val pullRequestLoader: PullRequestLoader,
        val pullRequestRepository: PullRequestRepository) {

    @RequestMapping("/list", "")
    fun list(): ResponseEntity<Map<String, Any>>? {
        val repoNames = ArrayList(repoLoader.availableRepos());

        val existing = repoRepository.
                findByIdIn(repoNames).map({ it.id })

        val loadedRepoUrls = existing.map({ it ->
            val link = linkTo(RepoController::class.java).slash("refresh")
                    .toUriComponentsBuilder().queryParam("fullName", it).toUriString()

            val last = pullRequestRepository.lastPullRequestId(it)
            val size = pullRequestLoader.sizeInRepo(it)
            mutableMapOf<String, Any>("href" to link, "state" to "Loaded $last of $size")
        })
        val builder = ImmutableMap.builder<String, Any>()
        builder.put("loaded", loadedRepoUrls)

        repoNames.removeAll { it -> existing.contains(it) }

        val newRepoUrls = repoNames.map({ it ->
            linkTo(RepoController::class.java)
                    .slash("load").toUriComponentsBuilder().queryParam("fullName", it).toUriString()
        })
        builder.put("new", newRepoUrls)

        val build = builder.build()
        return ResponseEntity.ok<Map<String, Any>>(build)
    }


    @RequestMapping(value = "/refresh")
    fun refresh(fullName: String): ResponseEntity<String> {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(fullName))

        if (!repoRepository.exists(fullName)) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Repo doesn't exist")
        }

        async() { loadingService.refresh(fullName) }

        val redirectUrl = trackUrl(fullName)
        return ResponseEntity.status(HttpStatus.FOUND).header(HttpHeaders.LOCATION, redirectUrl).body(null)
    }

    @RequestMapping(value = "/load")
    fun loadRepo(fullName: String): ResponseEntity<Any?> {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(fullName))

        if (repoRepository.exists(fullName)) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("The repo already loaded")
        }

        if (pullRequestLoader.sizeInRepo(fullName) == 0L) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No pull request created in repo, nothing to scan")
        }

        async() { loadingService.load(fullName) }

        return ResponseEntity.status(HttpStatus.FOUND).header(HttpHeaders.LOCATION, trackUrl(fullName)).body(null)
    }

    private fun trackUrl(fullName: String) = linkTo(this.javaClass).slash("track").toUriComponentsBuilder().queryParam("fullName",
            fullName).toUriString()

    @RequestMapping(value = "/track")
    fun trackRepo(fullName: String): ResponseEntity<Map<String, Any>> {
        Preconditions.checkArgument(!Strings.isNullOrEmpty(fullName))

        val progress = RepoLoadingProgressTracker[fullName] ?:
                return ResponseEntity.status(HttpStatus.NOT_FOUND)
                        .header("Refresh", "2")
                        .body(mapOf("Status" to
                                "The loading may be starting in a few seconds... if not, please check the repository full name"))

        if (progress.isFinished) {
            val statUrl = linkTo(HomeController::class.java).toUriComponentsBuilder().toUriString()
            RepoLoadingProgressTracker.remove(fullName)

            return ResponseEntity.status(HttpStatus.OK)
                    .body(mapOf(
                            "Status" to "Loading is finished, took ${progress.timeLog}",
                            "home" to statUrl)
                    )
        }


        return ResponseEntity.status(HttpStatus.OK).header("Refresh", "0.3")
                .body(mapOf(
                        "current" to progress.get(),
                        "total" to progress.size,
                        "bar" to progress.bar()
                ))
    }

}
