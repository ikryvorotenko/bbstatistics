package com.bbstatistics.controller

import com.bbstatistics.repository.filter.Filters
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.hateoas.mvc.ControllerLinkBuilder
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/filters")
@Scope("request")
class FiltersController @Autowired constructor(val filters: Filters) {

    @RequestMapping
    fun get(): ResponseEntity<Filters> {
        filters.repository
        return ResponseEntity.ok(filters)
    }

    @RequestMapping("/clear")
    fun clear(): ResponseEntity<Any?> {
        this.filters.clear();
        return redirectToRoot()
    }

    private fun redirectToRoot(): ResponseEntity<Any?> {
        val linkTo = ControllerLinkBuilder.linkTo(this.javaClass).toUriComponentsBuilder().toUriString();
        return ResponseEntity
                .status(HttpStatus.FOUND)
                .header(HttpHeaders.LOCATION, linkTo).body(null)
    }

    @RequestMapping("/set")
    fun set(filters: Filters): ResponseEntity<Any?> {
        this.filters.set(filters)
        return redirectToRoot()
    }

    @RequestMapping("/update")
    fun update(filters: Filters): ResponseEntity<Any?> {
        this.filters.update(filters)
        return redirectToRoot()
    }
}