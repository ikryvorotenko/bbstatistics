FROM java:8
VOLUME /tmp
ADD build/libs/bbstatistics-0.1-SNAPSHOT.jar app.jar
ADD oauth2.yml oauth2.yml

CMD java -Dspring.config.location=/oauth2.yml -jar /app.jar