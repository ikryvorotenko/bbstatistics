package com.bbstatistics.loader.bitbucket

import com.bbstatistics.loader.bitbucket.data.hal.PagedResource
import com.bbstatistics.util.async
import org.springframework.core.ParameterizedTypeReference
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder.fromHttpUrl
import java.util.*

abstract class PageLoader<T>(
        restTemplate: RestTemplate,
        type: ParameterizedTypeReference<T>,
        protected val pageType: ParameterizedTypeReference<PagedResource<T>>
) : Loader<T>(restTemplate, type) {

    protected fun loadAllPages(href: String): Collection<T> {
        val resource = loadPage(fromHttpUrl(href).queryParam("pagelen", 50).toUriString())

        if (resource.size == null) {
            return loadSubsequently(resource)
        }

        if (resource.size == 1L) {
            return resource.values;
        }

        return loadParallel(href, resource)
    }

    private fun loadPage(url: String): PagedResource<T> {
        return get(url, pageType).body
    }

    private fun loadSubsequently(page: PagedResource<T>): Collection<T> {
        var tempPage: PagedResource<T> = page

        val values = ArrayList(tempPage.values)
        while (tempPage.next != null) {
            tempPage = loadPage(tempPage.next!!)
            values.addAll(tempPage.values)
        }

        return values
    }

    private fun loadParallel(href: String, page: PagedResource<T>): Collection<T> {
        val pages = Math.toIntExact(page.size!! / page.pagelen!!) +
                if (page.size!! % page.pagelen!! == 0L) 0 else 1

        return async(1..pages + 1) {
            val url = fromHttpUrl(href).queryParam("page", it).queryParam("pagelen", page.pagelen).toUriString()
            loadPage(url)
        }.flatMap { it.values }

    }

}

