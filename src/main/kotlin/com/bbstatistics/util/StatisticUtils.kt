package com.bbstatistics.util

data class SummaryStatisticsInt(var count: Int = 0, var sum: Int = 0, var min: Int = Int.MAX_VALUE, var max: Int = Int.MIN_VALUE, var avg: Double = 0.0) : Comparable<SummaryStatisticsInt> {
    override fun compareTo(other: SummaryStatisticsInt): Int {
        return this.count.compareTo(other.count);
    }

    fun accumulate(newInt: Int): SummaryStatisticsInt {
        count++
        sum += newInt
        min = min.coerceAtMost(newInt)
        max = max.coerceAtLeast(newInt)
        avg = sum.toDouble() / count
        return this
    }
}

inline fun <T : Any> Collection<T>.summarizingInt(transform: (T) -> Int): SummaryStatisticsInt =
        this.fold(SummaryStatisticsInt()) { stats, item -> stats.accumulate(transform(item)) }

data class SummaryStatisticsLong(var count: Long = 0,
                                 var sum: Long = 0,
                                 var min: Long = Long.MAX_VALUE,
                                 var max: Long = Long.MIN_VALUE,
                                 var avg: Double = 0.0) : Comparable<SummaryStatisticsLong> {

    override fun compareTo(other: SummaryStatisticsLong): Int {
        return this.count.compareTo(other.count);
    }

    fun accumulate(newLong: Long): SummaryStatisticsLong {
        count++
        sum += newLong
        min = min.coerceAtMost(newLong)
        max = max.coerceAtLeast(newLong)
        avg = sum.toDouble() / count
        return this
    }
}

inline fun <T : Any> Collection<T>.summarizingLong(transform: (T) -> Long): SummaryStatisticsLong =
        this.fold(SummaryStatisticsLong()) { stats, item -> stats.accumulate(transform(item)) }
