package com.bbstatistics.loader.bitbucket.data

import com.bbstatistics.data.Comment
import com.bbstatistics.loader.bitbucket.data.hal.LinkedResource
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import java.util.*

class CommentView : LinkedResource() {

    var id: Long? = null

    @JsonDeserialize(using = ParentCommentDeserializer::class)
    var parent: Long? = null

    @JsonProperty("created_on")
    @JsonDeserialize(using = DateDeserializer::class)
    var createdOn: Date? = null

    @JsonProperty("updated_on")
    @JsonDeserialize(using = DateDeserializer::class)
    var updatedOn: Date? = null

    var user: UserView? = null

    @JsonDeserialize(using = ContentJsonDeserializer::class)
    var content: String? = null

    fun toComment(): Comment {
        return Comment(
                commentId = id!!,
                user = user!!.username,
                createdOn = createdOn!!,
                updatedOn = updatedOn!!,
                content = content!!,
                parent = parent
        )
    }

}
