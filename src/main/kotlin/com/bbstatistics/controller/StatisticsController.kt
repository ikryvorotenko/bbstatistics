package com.bbstatistics.controller

import com.bbstatistics.controller.statistics.ApprovalController
import com.bbstatistics.controller.statistics.CommentController
import com.bbstatistics.controller.statistics.PullrequestController
import org.springframework.context.annotation.Scope
import org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/stat")
@Scope("request")
open class StatisticsController {

    @RequestMapping
    fun meta(): LinksResource {
        return LinksResource(linkTo(CommentController::class.java).withRel("comment"),
                linkTo(ApprovalController::class.java).withRel("approval"),
                linkTo(PullrequestController::class.java).withRel("pullrequest"))
    }


}
