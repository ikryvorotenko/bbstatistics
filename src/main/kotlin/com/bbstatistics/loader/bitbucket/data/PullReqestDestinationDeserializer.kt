package com.bbstatistics.loader.bitbucket.data

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.TreeNode
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.node.NullNode
import java.io.IOException

class PullReqestDestinationDeserializer : JsonDeserializer<String>() {
    @Throws(IOException::class)
    override fun deserialize(jp: JsonParser, ctxt: DeserializationContext): String? {
        val codec = jp.codec
        val treeNode = codec.readTree<TreeNode>(jp)

        val repository = treeNode.get("repository")

        if (repository == NullNode.getInstance()) {
            return null
        }

        return repository.get("full_name").traverse(codec).readValueAs(String::class.java)
    }
}
