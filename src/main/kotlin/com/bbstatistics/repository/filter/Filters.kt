package com.bbstatistics.repository.filter

import com.bbstatistics.loader.bitbucket.data.State
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

data class Filters(private var map: MutableMap<String, Any?> =
                   mutableMapOf<String, Any?>().withDefault({ null })) {

    var repository: String? by map
    var from: Date? by map
    var to: Date? by map
    var date: Date? by map
    var user: String? by map
    var period: Period? by map
    var state: State? by map
    var team: String? by map

    fun set(filters: Filters) {
        map.clear();
        map.putAll(filters.map)
    }

    fun update(filters: Filters) {
        map.putAll(filters.map)
    }

    fun clear() {
        map.clear()
    }

    fun toQueryParams(): MultiValueMap<String, String>? {
        val params = LinkedMultiValueMap<String, String>()
        repository?.let { params.put("repository", listOf(repository)) }
        date?.let { params.put("date", listOf(DateTimeFormatter.ISO_DATE.format(date!!.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()))) }
        from?.let {
            params.put("from", listOf(DateTimeFormatter.ISO_DATE.format(from!!.toInstant().atZone(ZoneId.systemDefault()).toLocalDate())))
        }
        to?.let { params.put("to", listOf(DateTimeFormatter.ISO_DATE.format(to!!.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()))) }
        user?.let {
            params.put("user", listOf(user))
        }
        period?.let {
            params.put("period", listOf(period.toString()))
        }
        state?.let {
            params.put("state", listOf(state.toString()))
        }
        team?.let {
            params.put("team", listOf(team))
        }
        return params
    }

}