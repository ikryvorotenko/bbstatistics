package com.bbstatistics.loader.bitbucket.data

enum class State {
    OPEN,
    MERGED,
    DECLINED;

    companion object {
        fun forName(key: String): State {
            return State.values().find { it.name.equals(key, true) }
                    ?: throw IllegalArgumentException("No value found for ${key}")
        }

    }
}
