package com.bbstatistics.controller.statistics

import com.bbstatistics.controller.MetaController
import com.bbstatistics.data.PullRequest
import com.bbstatistics.loader.bitbucket.data.State
import com.bbstatistics.repository.PullRequestRepository
import com.bbstatistics.repository.filter.Filters
import com.bbstatistics.util.summarizingLong
import com.github.salomonbrys.kotson.jsonArray
import com.github.salomonbrys.kotson.jsonObject
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.mongodb.DBObject
import com.mongodb.util.JSON
import org.apache.commons.lang.time.DurationFormatUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.aggregation.Aggregation
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.Duration

@RestController
@RequestMapping("/stat/pullrequest")
@Scope("request")
open class PullrequestController @Autowired constructor(
    var repository: PullRequestRepository,
    val mongoTemplate: MongoTemplate) : MetaController() {

    @RequestMapping(value = "/comments-by")
    fun commentsOverAll(filters: Filters): Map<String, Int> {
        val pullRequests = repository.find(filters)
        val userComments = pullRequests.flatMap({ it ->
            it.comments.filter({ comment -> comment.user != it.author })
        })

        return userComments.groupBy({ it.user })
            .entries.sortedBy { it.value.size }
            .map { Pair(it.key, it.value.size) }
            .toMap()
    }

    @RequestMapping(value = "/by-user")
    open fun byUser(filters: Filters): MutableList<Map<*, *>>? {
        val criteria = repository.criteria(filters)

        val aggregate = mongoTemplate.aggregate(
            Aggregation.newAggregation(
                Aggregation.match(criteria),
                Aggregation.group("author").count().`as`("total"),
                Aggregation.project("total").and("author").previousOperation(),
                Aggregation.sort(Sort.Direction.DESC, "total")
            ),
            PullRequest::class.java,
            Map::class.java
        )

        return aggregate.mappedResults

    }

    @RequestMapping(value = "/by-days")
    fun pullrequests(filters: Filters): Iterable<*> {
        val criteria = repository.criteria(filters)

        Aggregation.newAggregation(Aggregation.match(criteria)).toString()

        //temporary workaround until https://jira.spring.io/browse/DATAMONGO-1299
        //gets resolved
        val jsonObject = jsonArray(
            Gson()
                .fromJson(
                    Aggregation.match(criteria).toDBObject(Aggregation.DEFAULT_CONTEXT).toString(),
                    JsonObject::class.java
                ),
            jsonObject(
                "\$project" to jsonObject(
                    "yearMonthDay" to jsonObject(
                        "\$dateToString" to jsonObject("format" to "%Y-%m-%d", "date" to "\$createdOn")
                    )
                )),
            jsonObject("\$group" to jsonObject(
                "_id" to "\$yearMonthDay",
                "total" to jsonObject("\$sum"  to 1)
            )),
            jsonObject(
                "\$project" to jsonObject("day" to "\$_id", "total" to 1, "_id" to 0)
            )
            ,
            jsonObject("\$sort" to jsonObject("day" to -1))
        )

        return mongoTemplate.db
            .getCollection("pullRequest")
            .aggregate(JSON.parse(jsonObject.toString()) as List<DBObject>)
            .results()
    }

    @RequestMapping(value = "/merged-by")
    fun mergedBy(filters: Filters): Map<String, Int> {
        val pullRequests = repository.find(filters)

        val pullrequestsByDates = pullRequests
            .filter { it.state == State.MERGED }
            .groupBy {
                it.updates.find { it.state == State.MERGED }!!.author
            }

        return pullrequestsByDates
            .entries.sortedBy { it.value.size }
            .map { Pair(it.key, it.value.size) }
            .toMap()
    }

    @RequestMapping(value = "/comments-on")
    fun commentsOnPullrequests(filters: Filters): Map<String, Int> {
        val pullRequests = repository.find(filters)

        val nonAuthorCommentsOnPullRequests = pullRequests.map({ it ->
            val author = it.author
            val comments = it.comments.filter({ comment -> comment.user != author })
            Pair(author, comments)
        })

        val result = nonAuthorCommentsOnPullRequests
            .groupBy { it.first }
            .entries.sortedBy { it.value.size }
            .map { Pair(it.key, it.value.size) }
            .toMap()

        return result
    }

    @RequestMapping(value = "/duration")
    open fun duration(filters: Filters): Map<String, Any> {
        val all = repository.find(filters)

        val durations = all.map { pullRequest ->
            val createdOn = pullRequest.createdOn
            val mergedOn = if (pullRequest.state == State.MERGED) pullRequest.updatedOn else null;

            val link = "https://bitbucket.org/${pullRequest.repo}/pull-requests/${pullRequest.id}"
            mergedOn?.let {
                val duration = Duration.between(createdOn!!.toInstant(), mergedOn.toInstant())
                PullRequestTime(link, duration.toMillis())
            } ?: PullRequestTime(link, 0L)
        }

        if (durations.isEmpty()) {
            return mapOf("total pull requests" to 0)
        }

        val statistics = durations.summarizingLong { it.duration }
        val result = mutableMapOf<String, Any>(
            "stat" to mutableMapOf(
                "average duration" to format(statistics.avg.toLong()),
                "total pull requests" to statistics.count,
                "max duration" to format(statistics.max),
                "min duration" to format(statistics.min)
            )
        )

        return result
    }

    @RequestMapping("/delays")
    open fun delays(filters: Filters): Map<String, Any> {
        val all = repository.find(filters)

        val delays = all.flatMap {
            val result = mutableListOf<Long>()

            val sortedComments = it.comments.sortedBy { it.createdOn }
            sortedComments.forEachIndexed { i, update ->
                val nextUpdate = sortedComments.toMutableList().getOrNull(i + 1) ?: return@forEachIndexed
                val duration = Duration.between(update.createdOn.toInstant(), nextUpdate.createdOn.toInstant())
                update.user.equals(nextUpdate.user)
                result.add(duration.toMillis())
            }
            result
        }
        val delaySummary = delays.summarizingLong { it }

        return mutableMapOf(
            "total pull requests" to all.size,
            "average delays" to format(delaySummary.avg.toLong()),
            "max delay" to format(delaySummary.max),
            "min delay" to format(delaySummary.min),
            "all comments durations" to delays.size)
    }

    fun format(duration: Long): String {
        return DurationFormatUtils.formatDurationWords(duration, true, true)
    }

    data class PullRequestTime(val link: String?,
        val duration: Long)
}