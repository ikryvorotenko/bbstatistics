package com.bbstatistics.loader.bitbucket.data

import com.bbstatistics.data.Update
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import java.util.*

class UpdateView {

    var author: UserView? = null
    var title: String? = null
    var state: State? = null

    @JsonDeserialize(using = DateDeserializer::class)
    var date: Date? = null

    @JsonProperty("source")
    @JsonDeserialize(using = SourceDeserializer::class)
    var commitView: CommitView? = null

    fun toUpdate(): Update {
        return Update(
                author = author!!.username,
                commitHash = commitView!!.hash!!,
                date = date!!,
                state = State.valueOf(state!!.toString())
        )
    }

}
