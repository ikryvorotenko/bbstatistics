package com.bbstatistics.service

import java.util.*
import java.util.concurrent.atomic.AtomicInteger

object RepoLoadingProgressTracker {

    private val tracks = HashMap<String, RepoLoadingProgress>()

    fun track(name: String, total: Int) {
        val progress = RepoLoadingProgress(total)
        tracks.put(name, progress)
    }

    fun increment(name: String) {
        tracks[name]?.increment()
    }

    operator fun get(name: String): RepoLoadingProgress? {
        return tracks[name]
    }

    fun remove(name: String): RepoLoadingProgress? {
        return tracks.remove(name)
    }

    class RepoLoadingProgress(var size: Int) {
        private val barSize = 50

        private var progress = AtomicInteger()

        var timeLog: String? = null
        var isFinished = false

        fun finish(timeLog: String) {
            this.timeLog = timeLog
            this.isFinished = true
        }

        fun increment() = progress.incrementAndGet();

        fun get() = progress.get();


        fun bar(): String {
            val progressBar = StringBuilder(barSize)

            val percent = progress.get() * 100 / size
            val current = percent * barSize / 100

            progressBar.append("[")
            (1..current).forEach {
                progressBar.append("■")
            }
            (current..barSize).forEach {
                progressBar.append(".")
            }
            progressBar.append("]")

            return "$percent% $progressBar"
        }
    }

}


