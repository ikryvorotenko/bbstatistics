package com.bbstatistics.data

import com.bbstatistics.loader.bitbucket.data.State
import org.springframework.data.mongodb.core.index.CompoundIndex
import org.springframework.data.mongodb.core.index.CompoundIndexes
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document
class Repo(
    var id: String,
    var uuid: String,
    var language: String,
    var createdOn: Date,
    var updatedOn: Date?,
    val isPrivate: Boolean
)

@Document
@CompoundIndexes(
    CompoundIndex(def = "{'repo': 1, 'author': 1, 'createdOn':1, 'state': 1}")
)
class PullRequest(
    var id: String?,
    var pullRequestId: Long,
    @Indexed
    var repo: String,
    @Indexed
    var author: String,
    @Indexed
    var createdOn: Date,
    @Indexed
    var state: State,
    var closedBy: String?,
    var updatedOn: Date?,
    var title: String?,
    var comments: Collection<Comment>,
    var updates: Collection<Update>,
    var approvals: Collection<Approval>
)

class Update(
    var date: Date?,
    var author: String,
    var state: State?,
    var commitHash: String
)

data class Comment(
    var commentId: Long,
    var user: String,
    var createdOn: Date,
    var updatedOn: Date?,
    var content: String?,
    var parent: Long?
)

data class Approval(
    var user: String,
    var date: Date
)

class User(
    var username: String,
    var displayName: String,
    var avatar: String
)