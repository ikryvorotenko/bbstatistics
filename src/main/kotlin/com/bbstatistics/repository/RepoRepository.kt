package com.bbstatistics.repository

import com.bbstatistics.data.Repo
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource
interface RepoRepository : MongoRepository<Repo, String> {
    fun findByIdIn(fullNames: List<String>): List<Repo>
}
