package com.bbstatistics.controller

import com.bbstatistics.loader.bitbucket.data.State
import com.bbstatistics.repository.filter.Period
import org.springframework.web.bind.WebDataBinder
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.InitBinder
import java.beans.PropertyEditorSupport
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

@ControllerAdvice
class ControllerBindings {

    @InitBinder
    fun initBinder(binder: WebDataBinder) {
        binder.registerCustomEditor(Date::class.java,
                object : PropertyEditorSupport() {
                    override fun setAsText(dateString: String) {
                        this.value = Date.from(LocalDate.parse(dateString,
                                DateTimeFormatter.ISO_DATE).atStartOfDay()
                                .atZone(ZoneId.of("GMT")).toInstant());
                    }
                })

        binder.registerCustomEditor(Period::class.java,
                object : PropertyEditorSupport() {
                    override fun setAsText(p0: String) {
                        this.value = Period.forName(p0);
                    }
                })

        binder.registerCustomEditor(State::class.java,
                object : PropertyEditorSupport() {
                    override fun setAsText(p0: String) {
                        this.value = State.forName(p0);
                    }
                })
    }

}