package com.bbstatistics.repository

import com.bbstatistics.data.PullRequest
import com.bbstatistics.repository.filter.Filters
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.repository.query.Param
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.stereotype.Repository
import java.time.ZoneId
import java.util.*

@RepositoryRestResource
interface PullRequestRepository : MongoRepository<PullRequest, String>, PullRequestFilteredRepository {
    fun findByRepo(@Param("repo") repo: String, p: Pageable): List<PullRequest>
    fun findByRepoAndPullRequestId(
        @Param("repo") repo: String,
        @Param("pullRequestId") pullRequestId: Long
    ): PullRequest
}

interface PullRequestFilteredRepository {
    fun lastPullRequestId(fullName: String): Long
    fun find(filters: Filters): List<PullRequest>
    open fun criteria(filters: Filters): Criteria?
}

@Repository
open class PullRequestRepositoryImpl
@Autowired constructor(val mongoTemplate: MongoTemplate) : PullRequestFilteredRepository {

    override fun lastPullRequestId(fullName: String): Long {
        val query = Query(Criteria("repo").`is`(fullName))
            .with(Sort(Sort.Order(Sort.Direction.DESC, "pullRequestId")))
            .limit(1)
        query.fields().include("pullRequestId")

        var result = 0L;
        mongoTemplate.executeQuery(query, "pullRequest", {
            result = it.get("pullRequestId") as Long;
        })

        return result
    }

    override fun find(filters: Filters): List<PullRequest> {
        val criteria = criteria(filters)
        return mongoTemplate.find(Query(criteria), PullRequest::class.java)
    }

    override fun criteria(filters: Filters): Criteria? {
        var criterias = mutableListOf<Criteria>();

        //TODO: finish team configuration
        filters.team?.let {
            criterias.add(Criteria.where("username").`in`())
        }

        if (filters.state != null) {
            criterias.add(Criteria.where("state").`is`(filters.state))
        }

        if (filters.period != null) {
            criterias.add(Criteria.where("createdOn").gt(filters.period!!.ago()))
        } else if (filters.date != null) {
            criterias.add(Criteria.where("createdOn").gte(filters.date))
            val nextDay = Date.from(filters.date!!.toInstant()!!.atZone(ZoneId.of("GMT")).plusDays(1).toInstant())
            criterias.add(Criteria.where("createdOn").lte(nextDay))
        } else {
            filters.from?.let {
                criterias.add(Criteria.where("createdOn").gt(it))
            }
            filters.to?.let {
                criterias.add(Criteria.where("createdOn").lt(it))
            }
        }

        filters.repository?.let {
            criterias.add(Criteria.where("repo").`is`(it))
        }

        filters.user?.let {
            criterias.add(Criteria.where("author").`is`(it))
        }

        val criteria = if (criterias.isEmpty()) Criteria() else Criteria().andOperator(*criterias.toTypedArray())
        return criteria
    }
}
