package com.bbstatistics.loader.bitbucket.data

import com.bbstatistics.loader.bitbucket.data.hal.LinkedResource
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import java.util.*

class RepoView(
        @JsonProperty("uuid")
        val uuid: String,

        @JsonProperty("full_name")
        val fullName: String,

        @JsonProperty("language")
        val language: String,
        @JsonProperty("created_on")
        @JsonDeserialize(using = DateDeserializer::class)
        val createdOn: Date = Date(),

        @JsonProperty("is_private")
        val isPrivate: Boolean
) : LinkedResource() {

    @JsonProperty("updated_on")
    @JsonDeserialize(using = DateDeserializer::class)
    val updatedOn: Date? = null
}
