package com.bbstatistics.controller

import com.bbstatistics.repository.filter.Filters
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.hateoas.Link
import org.springframework.hateoas.mvc.ControllerLinkBuilder
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestMapping
import java.lang.reflect.Method

open class MetaController() {

    @Autowired
    var filters: Filters = Filters()

    @RequestMapping
    fun meta(): LinksResource {
        val links = mappedMethods().map {
            val name = name(it)

            if (it.name.equals("dashboard")) {
                val builder = ControllerLinkBuilder.linkTo(this.javaClass).slash("dashboard").toUriComponentsBuilder()
                builder.queryParams(filters.toQueryParams())
                return@map Link(builder.toUriString(), "dashboard")
            }

            if (!hasFilters(it)) {
                return@map ControllerLinkBuilder.linkTo(it).withRel(name)
            }

            val builder = ControllerLinkBuilder.linkTo(it).toUriComponentsBuilder()
            builder.queryParams(filters.toQueryParams())
            Link(builder.toUriString(), name)
        }
        return LinksResource(links)
    }

    @RequestMapping(value = "/dashboard")
    fun dashboard(filters: Filters): ResponseEntity<MutableMap<String, Any>> {
        val methods = mappedMethods()

        //todo refactor this
        val mergedFilters = this.filters.copy()
        mergedFilters.update(filters)

        val result = mutableMapOf<String, Any>();
        methods.filter { !it.name.equals("dashboard") }.forEach {
            var name = name(it);

            if (hasFilters(it) && it.parameterCount == 1) {
                result.put(name, it.invoke(this, mergedFilters))
            } else if (it.parameterCount == 0) {
                result.put(name, it.invoke(this))
            }
        }

        return ResponseEntity.ok(result);

    }

    private fun hasFilters(it: Method) = it.parameters.filter { it.type.equals(Filters::class.java) }.isNotEmpty()

    private fun name(it: Method): String {
        val annotation = it.getAnnotation(RequestMapping::class.java)
        return annotation.value[0]
    }

    private fun mappedMethods(): List<Method> {
        return this.javaClass.methods
                .filter {
                    try {
                        val annotation = it.getAnnotation(RequestMapping::class.java)
                        annotation != null
                                && !annotation.value.isEmpty()
                    } catch (e: Exception) {
                        false;
                    }
                }
    }
}