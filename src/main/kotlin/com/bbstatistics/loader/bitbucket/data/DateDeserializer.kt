package com.bbstatistics.loader.bitbucket.data

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.TreeNode
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import java.io.IOException
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

class DateDeserializer : JsonDeserializer<Date>() {
    @Throws(IOException::class)
    override fun deserialize(jp: JsonParser, ctxt: DeserializationContext): Date {
        val codec = jp.codec
        val dateString = codec.readTree<TreeNode>(jp).traverse(codec).readValueAs(String::class.java)

        return Date.from(LocalDateTime.parse(dateString,
                DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSSXXX")).atZone(ZoneId.of("GMT")).toInstant())
    }
}
