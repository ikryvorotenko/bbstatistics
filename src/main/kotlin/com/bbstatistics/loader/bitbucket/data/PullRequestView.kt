package com.bbstatistics.loader.bitbucket.data

import com.bbstatistics.data.Approval
import com.bbstatistics.data.Comment
import com.bbstatistics.data.PullRequest
import com.bbstatistics.data.Update
import com.bbstatistics.loader.bitbucket.data.hal.LinkedResource
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import java.util.*

class PullRequestView(
    @JsonProperty("id")
    var id: Long,
    @JsonProperty("created_on")
    @JsonDeserialize(using = DateDeserializer::class)
    var createdOn: Date,
    @JsonProperty("author")
    var author: UserView
) : LinkedResource() {

    @JsonProperty(value = "destination")
    @JsonDeserialize(using = PullReqestDestinationDeserializer::class)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    var destinationRepository: String? = null

    var state: State? = null

    var title: String? = null

    @JsonProperty("closed_by")
    var closedBy: UserView? = null

    @JsonProperty("updated_on")
    @JsonDeserialize(using = DateDeserializer::class)
    var updatedOn: Date? = null

    var participants: MutableCollection<ParticipantView> = ArrayList()

    var activities: MutableCollection<ActivityView> = ArrayList()

    companion object {
        val REL_COMMENTS = "comments"
        val REL_COMMITS = "commits"
        val REL_ACTIVITY = "activity"
    }

    fun toPullRequest(
        repo: String,
        approvals: ArrayList<Approval>,
        comments: ArrayList<Comment>,
        updates: ArrayList<Update>
    ): PullRequest {
        return PullRequest(
            id = null,
            pullRequestId = id,
            repo = repo,
            updatedOn = updatedOn,
            author = author.username,
            closedBy = closedBy?.username,
            createdOn = createdOn,
            state = State.valueOf(state.toString()),
            approvals = approvals,
            comments = comments,
            updates = updates,
            title = title
        )
    }

}
