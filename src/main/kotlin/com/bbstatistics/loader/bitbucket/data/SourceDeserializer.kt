package com.bbstatistics.loader.bitbucket.data

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.TreeNode
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.node.NullNode
import java.io.IOException

class SourceDeserializer : JsonDeserializer<CommitView>() {
    @Throws(IOException::class)
    override fun deserialize(jp: JsonParser, ctxt: DeserializationContext): CommitView? {
        val codec = jp.codec
        val treeNode = codec.readTree<TreeNode>(jp)

        val commit = treeNode.get("commit")


        if (commit == NullNode.getInstance()) {
            return null
        }
        return commit.traverse(codec).readValueAs(CommitView::class.java)
    }
}
