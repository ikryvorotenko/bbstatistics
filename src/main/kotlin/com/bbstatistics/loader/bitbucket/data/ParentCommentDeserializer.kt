package com.bbstatistics.loader.bitbucket.data

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.TreeNode
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import java.io.IOException

class ParentCommentDeserializer : JsonDeserializer<Long>() {
    @Throws(IOException::class)
    override fun deserialize(jp: JsonParser, ctxt: DeserializationContext): Long? {
        val codec = jp.codec
        val treeNode = codec.readTree<TreeNode>(jp)
        return codec.readValue(treeNode.get("id").traverse(codec), Long::class.java)
    }
}
