package com.bbstatistics.loader.bitbucket.data

class ParticipantView {
    var role: String? = null

    var user: UserView? = null

    var approved: Boolean? = null

}
