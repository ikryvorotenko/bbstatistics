package com.bbstatistics.loader.bitbucket

import org.apache.commons.codec.net.URLCodec
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder.fromHttpUrl

abstract class Loader<T>(private val restTemplate: RestTemplate, protected val type: ParameterizedTypeReference<T>) {

    protected fun <K> get(href: String, type: ParameterizedTypeReference<K>): ResponseEntity<K> {
        try {
            return restTemplate.exchange(cleanUpUrl(href), HttpMethod.GET, null, type)
        } catch (e: Throwable) {
            throw PageLoadingException("Loading resource by $href failed.", e)
        }
    }

    /**
     * This is required because of Spring auth rest template
     * adds an access_token to each request automatically.
     *
     * So before sending a request i make sure there's no extra
     * access_token in it.
     */
    private fun cleanUpUrl(url: String): String {
        val builder = fromHttpUrl(url)

        val map = LinkedMultiValueMap(
                builder.build().queryParams)
        map.remove("access_token")

        return URLCodec().decode(builder.replaceQueryParams(map).build().normalize().toUriString())
    }

    protected fun load(href: String): T {
        return loadWithResponse(href).body
    }

    protected fun loadWithResponse(href: String): ResponseEntity<T> {
        return get(href, type)
    }
}

class PageLoadingException : RuntimeException {
    constructor(exception: Throwable) : super(exception)

    constructor(message: String, e: Throwable) : super(message, e)
}
